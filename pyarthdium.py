import os, platform, sqlite3, datetime, sys, cgi

BLOGNAME = u"Radiophobia"
SUBTITLE = u"Fear the computers"




try:
    if sys.argv[1] == 'help':
        print '\n\nPyarthdium simple blog generator thing.\nArguments:\n\ncleardb: clears database\nregen: regenerates the HTML\n\n'
        sys.exit(1)
except SystemExit:
    sys.exit(1)
except:
    pass

if os.path.isfile("cache.db") == False:
    conn = sqlite3.connect('cache.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE posts (id INTEGER PRIMARY KEY AUTOINCREMENT, date text, user text, title text, post text)''')
    conn.commit()
    conn.close()

conn = sqlite3.connect('cache.db')
c = conn.cursor()




template = u"""<!doctype html>
<head>
  <title>"""+BLOGNAME+u"""</title>
  <style>
body {
  background-color: rgb(238, 241, 255);
  font-family: Lucida Console,Lucida Sans Typewriter,monaco,Bitstream Vera Sans Mono,monospace;
}

.post {
  color: white;
  padding-left: 3px;
  padding-right: 3px;
  border-radius: 2px;
}

.holder {
  background-color: rgba(0,0,0,0.9);
  color: white;
  padding-left: 3%;
  padding-right: 3%;
  padding-top: 1px;
  border-radius: 2px;
  width: 75%;
  margin: auto;
  margin-top: 1%;
}

.date {
  float:right;
}

.title {
  color: white;
  padding-left: 3px;
  padding-right: 3px;
  border-radius: 2px;
  text-align: center;
  font-size: 24px;
}

.toptitle {
  color: black;
  text-align: center;
  font-size: 48px;
  font-weight: bolder;
}

.content {
  padding-bottom: 3%;
}

.subtitle {
  text-align: center;
}

  </style>
</head>

<body>
<div class="toptitle">
  <p>
    """+BLOGNAME+u"""
  </p>
  <div class="subtitle">
    <p>
      """+SUBTITLE+u"""
    </p>
  </div>
</div>

"""

try:
    if sys.argv[1] == 'cleardb':
        os.remove('cache.db')
        index = open('index.html', 'w').close()
        print 'DB cleared'
        sys.exit(1)
except SystemExit:
    sys.exit(1)
except:
    pass

try:
    if sys.argv[1] == 'regen':
        index = open('index.html', 'w').close()
        index = open('index.html', 'a')
        results = c.execute('''SELECT * FROM posts ORDER BY id DESC''').fetchall()
        postlist = []
        postlist.append(template)

        for x in results:
            html =  u'''<div class="holder">
  <div class="post">
    <p>
      '''+cgi.escape(x[2])+u'''
      <span class="date">ID: '''+str(x[0])+u''' | '''+x[1]+u'''</span>
    </p>
  </div>
  <div class="title">
    <p>
      '''+cgi.escape(x[3])+u'''
    </p>
  </div>
  <div class="content">
    '''+cgi.escape(x[4]).replace('\n', '<br />')+u'''
  </div>
</div>
'''
            postlist.append(html)

        for x in postlist:
            index.write(x)
        print 'Regenerated'
        sys.exit(1)
except SystemExit:
    sys.exit(1)
except:
    pass

title = raw_input("What should this post be titled? ")
user = raw_input("Who is posting? ")

print "An editor will open. Write your post, save, then close it."

if platform.system() == "Windows":
    os.system('sample.html')
elif platform.system() == "Linux":
    os.system('/usr/bin/nano /tmp/tmphtml')
    tmpfile = open('/tmp/tmphtml')
    c.execute("INSERT INTO posts (date, user, title, post) VALUES (?,?,?,?)", (str(datetime.datetime.now())[:10],user,title,tmpfile.read()))
    os.remove("/tmp/tmphtml")

index = open('index.html', 'w').close()
index = open('index.html', 'a')
results = c.execute('''SELECT * FROM posts ORDER BY id DESC''').fetchall()
postlist = []
postlist.append(template)

for x in results:
    html =  u'''<div class="holder">
  <div class="post">
    <p>
      '''+cgi.escape(x[2])+u'''
      <span class="date">ID: '''+str(x[0])+u''' | '''+x[1]+u'''</span>
    </p>
  </div>
  <div class="title">
    <p>
      '''+cgi.escape(x[3])+u'''
    </p>
  </div>
  <div class="content">
    '''+cgi.escape(x[4]).replace('\n', '<br />')+u'''
  </div>
</div>
'''
    postlist.append(html)

for x in postlist:
    index.write(x)

conn.commit()
conn.close()